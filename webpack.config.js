const path = require('path');
var webpack = require("webpack");
var fs = require('fs');
var bIsProductionBuild = process.argv.indexOf("production") !== -1;
var bIsDemoBuild = process.argv.indexOf("demo") !== -1;
var srcDir = "src";
if(bIsDemoBuild)
{
    srcDir = "demo-src";
}

var plugins = [];//Webpack plugins
if(bIsProductionBuild)
{
    plugins.push(new webpack.optimize.DedupePlugin()); //Search for equal or similar files and deduplicate them in the output.
    plugins.push(new webpack.optimize.UglifyJsPlugin({minimize: true,sourceMap:false}));//Minify JS output
}

module.exports = 
{
    entry: './'+ srcDir +'/app/js/main.ts',
    output: 
    {
        filename: '[name].js',
        publicPath: "/assets/js/"
    },
    plugins: plugins,
    module:
    {
        loaders: 
        [
            { 
                test: /\.ts$/, 
                loader: 'ts-loader',
            },
            {
                test    : /\.html$/,
                loader: 'raw!html-minify'
            },
            { 
                test: /\.scss$/, 
                loader: 'ignore-loader' 
            }
        ]
    },

    resolve: 
    {
      extensions: ['', '.webpack.js', '.web.js', '.ts', '.js'],
      root: path.resolve(__dirname),
      alias: 
      {
          "ElementAbstract"                 : "src/app/js/system/ElementAbstract",
          "InlineClassLoader"               : "src/app/js/system/InlineClassLoader",
          "Base"                            : "src/app/js/system/base"
      }
    }
};

console.log("===")
if(bIsProductionBuild)
{
    //When in production mode, we wont include source maps, as we dont want people on the website snooping through the code easily.
    console.log("=== NOTE: Production mode used, removing source maps ===");
}
else
{
    //When in development mode,we create source maps, this makes it so when the .js file is exported you can still debug with the original TypeScript files in the browser debugger
    console.log("===  NOTE: INCLUDING SOURCE MAPS, run 'gulp production' to remove them ===");
    module.exports["devtool"] = "source-map";
}
console.log("===")
