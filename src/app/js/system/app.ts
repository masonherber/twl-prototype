console.log("app main");

import $ = require('jquery');

const ResponsiveSourceSwapper = require("src/app/js/modules/responsive-src-swapper");

class App {
  protected options:Object = null;

  constructor(options) {
    this.options = $.extend({}, options)
    // console.log("App class loaded");

    this.initialise();
  }

  private initialise()
  {

    $("body").trigger("page-loaded");

    new ResponsiveSourceSwapper();
  
  }

}

export = App;