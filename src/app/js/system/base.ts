console.log("base project");

abstract class Base
{
  constructor()
  {
      if(typeof this.constructor !== "undefined" && typeof this.constructor["name"] !== "undefined")
      {
          //Just echo out what was created
          console.log("---" + this.constructor["name"] + "---");
      }
  }

  /**
   * 
   * Log with the prefix of the class, so you can see where the log is coming from easily
   * 
   */
  protected log(_logData:any)
  {
    if(typeof this.constructor === "undefined" || typeof this.constructor["name"] === "undefined")
    {
      console.log(_logData);
      return;
    }
    
    var newLineError = (new Error);
    if(typeof newLineError.stack === "undefined" || typeof newLineError.stack.split === "undefined")
    {
      console.log(_logData);
      return;
    }

    var caller_line = newLineError.stack.split("\n")[4];
    console.log("Log: " + _logData);
    console.log(caller_line)
  }

}

export = Base;