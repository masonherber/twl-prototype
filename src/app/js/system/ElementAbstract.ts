import Base = require("./base");
import $ = require('jquery');

// console.log("element abstract project");
/** 
 * 
 * This class should be extended by any class that instantiates on run time on the web page itself
 * It stores options specific to this module / component type
 * 
 * Usage example (if a module has extended this Abstract Class: <div data-module="my-module" data-module-options='{"myOptionKey":"myOptionValue"}'></div>
 * 
 */
abstract class ElementAbstract extends Base
{
  protected $el:JQuery = null;
  protected options:Object = null;
  constructor(protected el:HTMLElement, options:Object, private type:string)
  {
    super();
    this.$el = $(el)
    var optionsOnElement = this.$el.data("ud-core-options") ? this.$el.data("ud-core-options") : this.$el.data(type + '-options');

    this.options = $.extend(
      true,
      this.options,
      options,
      optionsOnElement
    );
  }
}

export default ElementAbstract;