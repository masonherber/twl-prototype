class InlineClassLoader
{
  constructor()
  {
    console.log("---Loader---");
    this.initialise();
  }

  private initialise()
  {
    this.loadModules();
  }

  /**
   * 
   * Loads classes and passes in options with the data-module attribute
   * 
   * Example Usage: <div data-module="my-module" data-module-options='{"myOption":"myOptionValue"}'></div>
   * 
   */
  private loadModules()
  {
    const moduleElements = document.querySelectorAll('[data-module]')

    for (var i = 0; i < moduleElements.length; i++)
    {
      const el = moduleElements[i];
      const name = el.getAttribute('data-module');
      const requiredElement = require(`../modules/${name}`);
      const Module = requiredElement.default;
      new Module(el);
    }
  }

}

export default new InlineClassLoader();