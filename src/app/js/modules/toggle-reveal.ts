
// --
// BLANK
// --
//
// A default blank es6 class to be used as a
// guide on structure
//


// --
// USAGE
// --
//
// place usefull comments about usage here
// 

import $ = require('jquery');
import ElementAbstract from "src/app/js/system/ElementAbstract"

export default class ToggleReveal extends ElementAbstract {
    constructor(el, options) {
        super(el, $.extend(true, {}, options), 'module');
        this.initialise(el);
    }

    initialise(el) {
        var toggleLink = $(el);
        // console.log($(el));
        toggleLink.on('click', function(e){
            toggleLink.toggleClass('toggle-reveal');
        });
    }
}
