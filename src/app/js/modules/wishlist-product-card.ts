
// --
// BLANK
// --
//
// A default blank es6 class to be used as a
// guide on structure
//


// --
// USAGE
// --
//
// place usefull comments about usage here
// 

import $ = require('jquery');
import ElementAbstract from "src/app/js/system/ElementAbstract"

export default class WishlistProductCard extends ElementAbstract
{
	constructor(el, options)
	{
		
		super(el, $.extend(true, {}, options), 'module');
		this.initialise();
	}

	initialise()
	{
		this.$el.on('click', function (e) {
			e.preventDefault();
			console.log("wishlist click");
            $(this).toggleClass('is-active');
        });

	}

}
