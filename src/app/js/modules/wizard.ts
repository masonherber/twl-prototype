
// --
// BLANK
// --
//
// A default blank es6 class to be used as a
// guide on structure
//


// --
// USAGE
// --
//
// place usefull comments about usage here
// 

import $ = require('jquery');
import ElementAbstract from "src/app/js/system/ElementAbstract"

export default class Wizard extends ElementAbstract
{
	constructor(el, options)
	{
		super(el, $.extend(true, {}, options), 'module');
		this.initialise();
	}

	initialise()
	{
		
		this.$el.find(".js-wizard-link").on("mousedown", function(event){
			console.log("mousedown");
			// if($(this).hasClass("is-selected")) {
			// 	$(this).removeClass("is-selected");
			// 	$(this).removeClass("t-dark");
			// 	// removeClass("t-dark");
			// 	// $(this).find(".c-wizard-card__cta").addClass("t-dark");
			// } else {
				// $(this).addClass("is-selected");
				$(this).find(".c-wizard-card__cta").addClass("is-selected").addClass("t-dark");
			// }

		});
	}

}
