
// --
// BLANK
// --
//
// A default blank es6 class to be used as a
// guide on structure
//


import $ = require('jquery');
import 'slick-carousel';
import ElementAbstract from "../system/ElementAbstract";
import DebouncerWindowResize = require("./debouncer-window-resize");

export default class mobileCardsCarousel extends ElementAbstract
{
	
	private nextBtn: JQuery = null;
	private previousBtn: JQuery = null;
	private carouselElement:JQuery = null;
	private m_debounceWindowResize:DebouncerWindowResize = null; 
	private screenWidth:number = null;
	private breakpoint:number = null;
	private carouselTo:number = 0;
	private carouselInit:Boolean = false;
	
	constructor(el, options)
	{
		super(el, $.extend(true, {
			carouselTo: 767,
			carouselElement: ".js-carousel"
		}, options), 'module');
		this.initialise();
	}

	private initialise()
	{
		this.carouselElement = this.$el.find(this.options['carouselElement']);//this.$el.find(".js-deal-slide-carousel");
		this.carouselTo = this.options['carouselTo'];
		// console.log("this.carouselElement: ", this.carouselElement);

		this.m_debounceWindowResize = new DebouncerWindowResize(() => this.onWindowResizeCheckBreakPoints(),100);
		this.checkResponsive();

	}

	private onWindowResizeCheckBreakPoints()
    {
		// console.log("onWindowResizeSettled");	
		var windowSize = $(document).width();
			// console.log("screenWidth: ", this.screenWidth);

		if(this.breakpoint !== null && this.breakpoint === windowSize)
			{
			//No point in resizing the images, as they're based on the width of the browser
			//it may have been vertically resized, like scrolling on an ipad and the tab bar disappears
			return;
		}
		
		this.breakpoint = windowSize;

        
        this.checkResponsive();
	}

	private getCurrentScreenWidth():number {
		this.screenWidth = $(document).width();
		// console.log("this.screenWidth: ", this.screenWidth);
		return this.screenWidth;
	}

	private checkResponsive() {
		this.getCurrentScreenWidth();
		// console.log("checkResponsive");

		// console.log("this.screenWidth: ", this.screenWidth);
		// console.log("this.carouselInit: ", this.carouselInit);

		if(this.screenWidth <= this.carouselTo) {

			// console.log("screenWidth <= carouselTo");

			if(this.carouselInit === false) {
				// console.log("initialiseCarousel");
				this.initialiseCarousel();
			}
			
		} else {

			// console.log("screenWidth > carouselTo");

			if(this.carouselInit === true) {
				// console.log("unslick!!!!");
				this.carouselInit = false;
				this.$el.removeClass("is-active");
				this.carouselElement.unslick();
			}
		}
	}

	private initialiseCarousel() {
		
		this.carouselElement.slick({
			mobileFirst: true,
			infinite: false,
			arrows: false,
			rows: 0,
			responsive: [
				{
					breakpoint: 0,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				},
				// {
				// 	breakpoint: 400,
				// 	settings: {
				// 	  slidesToShow: 2,
				// 	  slidesToScroll: 1
				// 	}
				// },
				{
					breakpoint: 600,
					settings: {
					  slidesToShow: 3,
					  slidesToScroll: 3
					}
				},
				{
					breakpoint: 768,
					settings: 'unslick'
				}
			]
		});

		this.carouselInit = true;
		this.$el.addClass("is-active");
		  
	}


}
