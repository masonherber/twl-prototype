

import $ = require('jquery');
import 'slick-carousel';
import ElementAbstract from "../system/ElementAbstract"

export default class SlickProductCardsCarousel extends ElementAbstract
{
	
	private nextBtn: JQuery = null;
	private previousBtn: JQuery = null;
	private carouselElement:JQuery = null;
	private carouselInit:Boolean = false;
	
	constructor(el, options)
	{
		// console.log("blanky mc blank");
		super(el, $.extend(true, {}, options), 'module');
		this.initialise();
	}

	private initialise()
	{
		
		this.carouselElement = this.$el.find(".js-carousel");
		this.nextBtn = this.$el.find(".js-btn-next");
		this.previousBtn = this.$el.find(".js-btn-previous");

		if(this.carouselInit === false) {
			this.initialiseCarousel();
		}
		
	}


		

	private initialiseCarousel() {
		
		this.carouselElement.slick({
			mobileFirst: true,
			infinite: false,
			prevArrow: this.previousBtn,
			nextArrow: this.nextBtn,
			responsive: [
				{
					breakpoint: 0,
					settings: {
					  slidesToShow: 1,
					  slidesToScroll: 1
					}
				},
				// {
				// 	breakpoint: 400,
				// 	settings: {
				// 	  slidesToShow: 2,
				// 	  slidesToScroll: 1
				// 	}
				// },
				{
					breakpoint: 600,
					settings: {
					  slidesToShow: 3,
					  slidesToScroll: 3
					}
				},
				{
					breakpoint: 988,
					settings: {
					  slidesToShow: 4,
					  slidesToScroll: 4
					}
				},
				{
				  breakpoint: 1680,
				  settings: {
					slidesToShow: 5,
					slidesToScroll: 5
				  }
				}
			]

		});

		this.carouselInit = true;
		this.$el.addClass("is-active");

	}



}
