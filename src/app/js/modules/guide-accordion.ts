import $ = require('jquery');
import ElementAbstract from "../system/ElementAbstract";
//import ElementUtil = require("src/app/core/utils/ElementUtil");
// import ElementUtil = require("../utils/ElementUtil");

class guideAccordion extends ElementAbstract
{
    constructor(el:HTMLElement,options:Object)
    {
        super(el,$.extend(true, {
        }, options),'module')
        this.initialise();

        // console.log(ElementUtil.getElementsWithText(this.$el[0],"Michael's"));

    }


    private initialise()
    {
        this.initialiseListeners();
    }


    private initialiseListeners()
    {
        this.$el.click((e:MouseEvent) => this.onLiClicked(e));
    }

    private onLiClicked(e:MouseEvent)
    {
        var $target:JQuery = $(e.target);

        // if($target.closest(".js-cancel-child-accordion-click").length)
        // {
        //     return;
        // }

        var $closestLi:JQuery = $target.closest("li");
        if(!$closestLi.length)
        {
            return;
        }

        var closestLi:HTMLElement = $closestLi[0];
        if(closestLi.classList.contains("-active"))
        {
            closestLi.classList.remove("-active");
        }
        else
        {
            closestLi.classList.add("-active");
        }

    }


}

export default guideAccordion;