
// --
// Header Navigation
// --
//

// --
// USAGE
// --
//
// place usefull comments about usage here
// 

import $ = require('jquery');
import ElementAbstract from "src/app/js/system/ElementAbstract"

export default class ShowMore extends ElementAbstract {


    constructor(el, options) {
        super(el, $.extend(true, {
            numToShow: 8,
        }, options), 'module');
        console.log(options);
        this.initialise(el);
    }


    initialise(el) {       
        const scope = this;
        //const el = $(el);
        const numToShow = this.options['numToShow'];
        const activeClass = 'is-hidden';
        const listItems = $('>li', el);
        const showButton = $(el).next('.js-show-more-button');
        const totalInList = listItems.length;
        var numShowing = numToShow;

        if (totalInList <= numToShow) {
            showButton.hide();
        }
        listItems.slice(numToShow, totalInList).addClass(activeClass);

        showButton.on('click', function () {
            console.log(numShowing);
            if (showButton.text() === 'Show less') {
                listItems.slice(numToShow, totalInList).addClass(activeClass);
                showButton.text('Show more');
                numShowing = numToShow;
            } else {
                listItems.slice(numShowing - 1, numShowing + numToShow).removeClass(activeClass);
                numShowing = numShowing + numToShow;
                if (numShowing >= totalInList) {
                    showButton.text('Show less');
                }
            }
        });
    }
}
