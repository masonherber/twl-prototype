
// --
// Site tree reveal
// --
//
// The site tree module (displaying on the clp page).
// This controls hiding on mobile (for SEO purposes done in JS) and expands and collapses module
//

import $ = require('jquery');
import ElementAbstract from "src/app/js/system/ElementAbstract";
import DebouncerWindowResize = require("./debouncer-window-resize");

export default class SiteTreeReveal extends ElementAbstract {

    private revealBtn: JQuery = null;
    private revealWrap: JQuery = null;
    private m_debounceWindowResize:DebouncerWindowResize = null; 
    private screenWidth:number = null;
    private breakpoint:number = null;
    private defaultHeight:number = 300;

    constructor(el, options) {
        super(el, $.extend(true, {}, options), 'module');
        this.initialise(el);
    }

    initialise(el) {

        this.revealBtn = this.$el.find(".js-reveal-btn");
        this.revealWrap = this.$el.find(".js-reveal");

        this.m_debounceWindowResize = new DebouncerWindowResize(() => this.onWindowResizeCheckBreakPoints(),100);
        this.onWindowResizeCheckBreakPoints();

        this.revealBtn.click((e:any) => this.toggleSiteTreeHeight(e));
        
    }

    private toggleSiteTreeHeight(e) {
        // console.log("toggleSiteTreeHeight");
        e.preventDefault();
        var scope = this;
        if(scope.revealWrap.hasClass('toggle-reveal')) {
            // scope.revealWrap.removeAttr("style");
            scope.revealBtn.text("Show more");
            // scope.revealWrap.height(scope.$el.height());
            scope.revealWrap.removeClass('toggle-reveal');
            // scope.revealWrap.height(this.defaultHeight);
        }
        else  {
            // var wrapHeight = scope.$el.height();
            // console.log("this.$el.height: ", wrapHeight);
            // scope.revealWrap.height(wrapHeight);
            scope.revealWrap.addClass('toggle-reveal');
            scope.revealBtn.text("Show less");
        }
    }


    private onWindowResizeCheckBreakPoints() {
        var windowSize = $(document).width();
        // console.log("screenWidth: ", this.screenWidth);

        if(this.breakpoint !== null && this.breakpoint === windowSize)
            {
            return;
        }
        
        this.breakpoint = windowSize;

        this.showHideMobile();
    }

    private getCurrentScreenWidth():number {
		this.screenWidth = $(document).width();
		// console.log("this.screenWidth: ", this.screenWidth);
		return this.screenWidth;
    }
    
    private showHideMobile() {
		this.getCurrentScreenWidth();
		// console.log("showHideMobile");
		
		if(this.screenWidth < 768) {
            this.$el.addClass("is-hidden");
		} else {
            this.$el.removeClass("is-hidden");
        }
		
	}
}
