
// --
// Product Card Hover
// --
//
// The color swatch changer on product card swatch hover
//

import $ = require('jquery');
import ElementAbstract from "../system/ElementAbstract"

export default class ProductCardColorHover extends ElementAbstract
{
	private productColorSwatch:JQuery
	private colorTimeout;
	private $element:JQuery = null;
	private $swatchesWrap:JQuery;
	private $swatches:JQuery;
	private $imageWrap:JQuery;
	private $images:JQuery;

	constructor(el, options)
	{
		super(el, $.extend(true, {}, options), 'module');
		this.initialise();
	}

	initialise()
	{

		$(".js-product-color").hover((e:any) => this.productSwatchHover(e));
		$(".js-product-card-colors").mouseleave((e:any) => this.productSwatchMouseLeave(e));
	}

	private productSwatchHover(e:any) {
		clearTimeout(this.colorTimeout);
		
		var $swatch = $(e.currentTarget);

		this.$element = $swatch.parent().parent();
		this.$swatchesWrap = this.$element.find(".js-product-card-colors");
		this.$swatches = this.$swatchesWrap.find(".js-product-color");
		this.$imageWrap = this.$element.find(".c-card-product__image-wrap");
		this.$images = this.$imageWrap.find(".js-product-image");

		
		var imageClass = $swatch.data("color-image");
		// console.log("imageClass: ", imageClass);
		this.$swatches.removeClass("is-active");
		$swatch.addClass("is-active");
		this.$images.removeClass("is-active");
		this.$imageWrap.find("."+imageClass).addClass("is-active");
		// console.log("swatch: ", $swatch);
	}

	private productSwatchMouseLeave(e:any) {
		var scope = this;
		this.colorTimeout = setTimeout(function(){ 
			scope.$swatches.removeClass("is-active");
			scope.$swatchesWrap.parent().find(".is-default").addClass("is-active");
			scope.$images.removeClass("is-active");
			scope.$imageWrap.find(".is-default").addClass("is-active");
		}, 500);
	}

}
