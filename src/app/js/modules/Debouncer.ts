/**
 * 
 * == Debouncer ==
 * 
 * An extendable class which simply calls a callback when a timeout expires
 * Has the ability to reset and clear the timeout
 * 
 */

abstract class Debouncer
{
    private m_timeoutTimer:number = null; //Number reference to the current timeout
    constructor(private m_debounceCallback:Function, private m_debounceTimeMilliseconds:number)
    {

    }


    /**
     * 
     * Clear the current timeout timer if it's currently active and null the member variable
     * 
     */
    public clear()
    {
        if(null === this.m_timeoutTimer)
        {
            return;
        }
        
        window.clearTimeout(this.m_timeoutTimer);
        this.m_timeoutTimer = null;
    }


    /**
     * 
     * Reset the timeout timer, clears it first, then sets the timeout
     * 
     */
    public reset()
    {
        this.clear();
        this.m_timeoutTimer = window.setTimeout(() => this.onExpired(), this.m_debounceTimeMilliseconds);
    }


    /**
     * 
     * Called when setTimeout has expired, call the passed in callback function from the constructor
     * 
     */
    protected onExpired()
    {
        this.clear();
        //https://developers.google.com/web/fundamentals/performance/rendering/debounce-your-input-handlers
        requestAnimationFrame(<FrameRequestCallback>() => this.m_debounceCallback());
    }
}

export = Debouncer