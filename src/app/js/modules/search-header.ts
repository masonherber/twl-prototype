

import $ = require('jquery');
import ElementAbstract from "src/app/js/system/ElementAbstract"

export default class SearchHeader extends ElementAbstract
{
	constructor(el, options)
	{
		// console.log("mobilenav constructor");
		super(el, $.extend(true, {}, options), 'module');
		this.initialise();
		this.initialiseListeners();
	}

	initialise(){
    	
	}
	private initialiseListeners(){
		// console.log("search header", this.$el);
		$("#js-search-btn").click((e:MouseEvent) => this.onSearchToggle(e));
		$(".js-mobile-search-close-btn").click((e:MouseEvent) => this.onSearchToggle(e));
		//this.$el.find(
		$(".js-popover-submit-search-btn").click((e:MouseEvent) => this.onPopupSearchButtonClicked(e));
	}

	private onSearchToggle(e:MouseEvent)
    {
        e.preventDefault();
		// console.log("onSearchToggle", this.$el);

		//$(".js-header-search").addClass("-is-active");
		//var $currentTarget:HTMLElement = <HTMLElement>e.currentTarget;
        // var $headerSearchCtn = $(currentTarget).closest(".m-header");
		//$currentTarget.addClass("-is-active");
        if($(".js-header-search").hasClass("-is-active"))
        {
        //     //Search
        //     // if(bMobileHeader)
        //     // {
        //     //     //If we're on mobile then the search icon is a toggle.
        //     //     this.closeSearchHeader();
        //     // }
			this.closeSearchHeader();
        }
        else
        {
        //     //activate
			this.openAndFocusOnSearch();
			//$(document.activeElement).parent().previous().focus();
			//setTimeout(function() { $(".js-popup-search-input").focus() }, 3000);
			this.$el.find(".js-search-input").focus();
    	}
		
    }

	private openAndFocusOnSearch()
    {
        var $headerSearchCtn = $(".js-header-search");
		$headerSearchCtn.addClass("-is-active");
		//$("body").trigger("refresh-ud-sticky");
		$("body").addClass("u-no-scroll");
    }

	private closeSearchHeader()
    {
		var $headerSearchCtn = $(".js-header-search");
		$headerSearchCtn.removeClass("-is-active");
		//$("body").trigger("refresh-ud-sticky");
		$("body").removeClass("u-no-scroll");
	}

	private onPopupSearchButtonClicked(e:MouseEvent)
    {
        e.preventDefault();
        
    }

}
