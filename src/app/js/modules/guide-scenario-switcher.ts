
// --
// BLANK
// --
//
// A default blank es6 class to be used as a
// guide on structure
//


// --
// USAGE
// --
//
// place usefull comments about usage here
// 

import $ = require('jquery');
import ElementAbstract from "../system/ElementAbstract"

export default class ScenarioSwitcher extends ElementAbstract
{
	
	constructor(el, options)
	{
		console.log("mobilenav constructor");
		super(el, $.extend(true, {}, options), 'module');
		this.initialise();
		this.initialiseListeners();
	}

	initialise(){
    	
	}
	private initialiseListeners(){
		console.log("initialiseListeners", this.$el);
		
		//burger btn click
		$(".js-variant").click((e:MouseEvent) => this.onSelectChange(e));
	}

	private onSelectChange(e:MouseEvent){
		e.preventDefault();
		console.log(e.currentTarget);
		// this.$el.toggleClass("-guide-nav-active");
		// $("body").toggleClass("-guide-nav-active");
	}

}
