
// --
// BLANK
// --
//
// A default blank es6 class to be used as a
// guide on structure
//


// --
// USAGE
// --
//
// place usefull comments about usage here
// 

// import {Breakpoints} from "../../core/utils/Breakpoints";
// import {iBreakpoint} from "../../core/utils/Breakpoints"; 
// import ElementUtil = require("../../core/utils/ElementUtil");
import $ = require("jquery");
import ElementAbstract from "../system/ElementAbstract"

export default class Mobile_Nav extends ElementAbstract
{
    private readonly m_mediaElementSelector:string = "[data-responsive-src]";
	private readonly m_backgroundImageSelector:string = "[data-responsive-background-src]";
	private readonly m_bLazyLoadImages = false;
	private readonly LAZY_LOAD_PERCENTAGE_OF_WINDOW_HEIGHT = 10;

	constructor(el, options)
	{
		super(el, $.extend(true, {}, options), 'module');
		console.log("mobilenav constructor");
		this.initialise();
		this.initialiseListeners();
	}

	initialise(){
    	
	}
	private initialiseListeners(){
		console.log("initialiseListeners", this.$el);
		
		//burger btn click
		$(".js-guide-nav-trigger").click((e:MouseEvent) => this.onNavOpenClose(e));
		$(".js-nav-link-grand-child").click((e:MouseEvent) => this.onLinkClick(e));
		
		//close btn click
		// this.$el.find(".js-mobile-nav-close-btn").click((e:MouseEvent) => this.onNavOpenClose(e));
	}

	private onNavOpenClose(e:MouseEvent){
		e.preventDefault();
		this.$el.toggleClass("-guide-nav-active");
		$("body").toggleClass("-guide-nav-active");
	}

	private onLinkClick(e:MouseEvent){
		e.preventDefault();
		var $item = $(e.currentTarget);
		var loadUrl = $item.attr("href");
		var loadAction = $item.data("load");
		console.log("loadAction: "+loadAction);
		// if(loadAction === "ajax") {
		// 	$( ".g-load" ).load( loadUrl+" .g-main", function() {
		// 		$(".g-nav__link").removeClass("-active");
		// 		$item.addClass("-active");
		// 		$item.parent().parent().parent().find("> .g-nav__link").addClass("-active");
				
		// 	});

		// } else {
			window.location.href = loadUrl;
		// }

	}


}

