
// --
// BLANK
// --
//
// A default blank es6 class to be used as a
// guide on structure
//


// --
// USAGE
// --
//
// place usefull comments about usage here
// 

import $ = require('jquery');
import ElementAbstract from "src/app/js/system/ElementAbstract"

export default class Entry_Blank extends ElementAbstract
{
	constructor(el, options)
	{
		// console.log("blanky mc blank");
		super(el, $.extend(true, {}, options), 'module');
		this.initialise();
	}

	initialise()
	{
    	// console.log("init blank called : test");

	}

}
