
import $ = require("jquery");
import DebouncerWindowResize = require("./debouncer-window-resize");

/**
 * You should leave src blank, and leave it up to responsive-src
 * 
 * Sample usage: If you want mobile, tablet portrait and tablet landscape images / videos
 * Note: the biggest device image source will be the source from that breakpoint onwards
 * 

 * Note: this can also be used for background images
 * 
 *   <div data-responsive-background-src
 *          data-src='../../assets/images/data-src-xs.jpg'
 *        data-src-s='../../assets/images/data-src-s.jpg' 
 *        data-src-m='../../assets/images/data-src-m.jpg'  
 *        data-src-l='../../assets/images/data-src-l.jpg'
 *          data-src-xl='../../assets/images/data-src-xl.jpg' 
 *        >
 *   </div>
 * 
 */

class ResponsiveSourceSwapper
{
    private mediaElementSelector:string = "[data-responsive-src]";
    private backgroundImageSelector:string = "[data-responsive-background-src]";
    private breakpointsArray:Array<any> = [xs=>0, s=>600, m=>768, l=>988, xl=>1320, xxl=>1680];
    private breakpoint:number = 0;
    private screenWidth:number;
    private m_windowDebouncer:DebouncerWindowResize = null;
    

    constructor(el, options)
    {
        this.initialise();
        
    }

    public initialise()
    {
        this.checkImages();

        this.m_windowDebouncer = new DebouncerWindowResize(() => this.onWindowResizeSettled(), 500);
    }

    // private onWindowResized()
    // {
    //     requestAnimationFrame(() => this.checkImages());
    // }

    onWindowResizeSettled() 
    {
        // console.log("onWindowResizeSettled");
        var windowSize = $(window).width();

        if(this.breakpoint === windowSize)
        {
        //No point in resizing the images, as they're based on the width of the browser
        //it may have been vertically resized, like scrolling on an ipad and the tab bar disappears
        return;
        }

        this.breakpoint = windowSize;

        // console.log("onWindowResizeSettled");
        this.checkImages();

    }

    private getCurrentBreakpoint():number {
        this.breakpoint = parseInt(window.getComputedStyle(document.querySelector('body'), ':before').getPropertyValue('content').replace(/\"/g, ''));
        // console.log("this.breakpoint: ", this.breakpoint);
        return this.breakpoint;
    }

    private getCurrentScreenWidth():number {
        this.screenWidth = $(document).width();
        // console.log("this.screenWidth: ", this.screenWidth);
        return this.screenWidth;
    }
    
    private createImageArray() {
        var scope = this;
        
        [].forEach.call(document.querySelectorAll(this.backgroundImageSelector), function(element) {
            var $element = $(element);
            var imageArray = [];

            if($element.data("src")) {
                imageArray['0'] = $element.data("src");
            }
            if($element.data("src-s")) {
                imageArray['600'] = $element.data("src-s");
            }
            if($element.data("src-m")) {
                imageArray['768'] = $element.data("src-m");
            }
            if($element.data("src-l")) {
                imageArray['1024'] = $element.data("src-l");
            }
            if($element.data("src-xl")) {
                imageArray['1320'] = $element.data("src-xl");
            }
            if($element.data("src-xxl")) {
                imageArray['1680'] = $element.data("src-xxl");
            }
            // console.log("imageArray: ", imageArray);
            scope.compareImagesAndBreakpoints($element, imageArray);
        });
    }

    private compareImagesAndBreakpoints($image, imageArray) {
        var arrayLength = imageArray.length;
        var scope = this;
        var imageSrc = "";
        
        for (var k in imageArray){
            if(this.screenWidth >= parseInt(k)) {
                imageSrc = imageArray[k];
                // console.log("larger ");
                this.addBackgroundStyle($image, imageSrc);
            } else {

            }
        }

    }

    private addBackgroundStyle($image, imageSrc) {
        $image.attr("style", "background-image: url("+imageSrc+")");
        // console.log("imageSrc: ", imageSrc);
    }

    private checkImages()
    {
        this.getCurrentScreenWidth();
        this.createImageArray();
    }

}

export = ResponsiveSourceSwapper;
