
// --
// Header Navigation
// --
//

// --
// USAGE
// --
//
// place usefull comments about usage here
// 

import $ = require('jquery');
import ElementAbstract from "src/app/js/system/ElementAbstract"

export default class MobileMenu extends ElementAbstract {
    private itemHeight = 54;
    private activeClass = 'is-active';
    private activeEl;

    constructor(el, options) {
        super(el, $.extend(true, {}, options), 'module');
        this.initialise(el);
    }


    initialise(el) {
        var scope = this;
        //this.$el.
        // console.log(this.$el);
        const sidebar = $(el);
        var activeEl = sidebar.children('.is-active').first();
        const dropdownSiblings = sidebar.find('.m-mobilemenu-content a');
        const levelClass = 'menu-level';
        const mobileHeaderHeight = 140;
        const initExpandedHeight = (activeEl.children("ul").first().children("li").length - 1 + activeEl.find(".m-mobilemenu-shop").children("li").length) * scope.itemHeight + 'px';
        var initCollapsedHeight;
        console.log(initExpandedHeight);
        var firstTime = true;

        $('#js-nav-burger-menu,#js-nav-close-menu').on('click', function () {
            if(firstTime){
                initCollapsedHeight = $('.m-mobilemenu-content').css('height', $('.m-mobilemenu-content ul').height() + 'px');
                firstTime = false;
            }
            $('body').toggleClass('mobilemenu-show');
        });

        $(dropdownSiblings).on('click', function (e) {
            console.log($(this));
            if ($(this).attr('href') == '' || !$(this).attr('href')){
                    e.preventDefault();
                    e.stopPropagation();
                    if($(this).hasClass('no-menu')){
                        $('.m-mobilemenu-shop').parent().toggleClass('m-mobilemenu-shop-expanded');
                        if ($('.m-mobilemenu-shop').parent().hasClass('m-mobilemenu-shop-expanded')) {
                            $('.m-mobilemenu-content').css('height', initExpandedHeight);
                        } else {
                            activeEl = $(el).children('.is-active').first();
                            $('.m-mobilemenu-content').css('height', initCollapsedHeight);
                        }
                    }else{
                        if (sidebar.hasClass('sub-cat3')) {
                            sidebar.addClass('sub-cat4');
                        } else if (sidebar.hasClass('sub-cat2')) {
                            sidebar.addClass('sub-cat3');
                        } else if (sidebar.hasClass('sub-cat')) {

                            sidebar.addClass('sub-cat2');
                        } else {
                            sidebar.addClass('sub-cat');
                        }
                        activeEl = $(this).closest('li');
                        activeEl.addClass(scope.activeClass);
                        var _ul = activeEl.find('ul');
                        $('.m-mobilemenu-content').css('height', activeEl.children("ul").first().children("li").length * scope.itemHeight + 'px');
                        $('.m-mobilemenu-content').scrollTop(0);  
                    }
            }
        }); 
        $('.cat-back').on('click', function () {
            activeEl.removeClass(scope.activeClass);
            activeEl = $(this).parent().parent();
            var _ul = activeEl.find('ul');
            if (sidebar.hasClass('sub-cat4')) {
                sidebar.removeClass('sub-cat4');
                $('.m-mobilemenu-content').css('height', activeEl.children("ul").first().children("li").length * scope.itemHeight + 'px');
            } else if (sidebar.hasClass('sub-cat3')) {
                sidebar.removeClass('sub-cat3');
                $('.m-mobilemenu-content').css('height', activeEl.children("ul").first().children("li").length * scope.itemHeight + 'px');
            } else if (sidebar.hasClass('sub-cat2')) {
                sidebar.removeClass('sub-cat2');
                $('.m-mobilemenu-content').css('height', activeEl.children("ul").first().children("li").length * scope.itemHeight + 'px');
            } else {
                sidebar.removeClass('sub-cat');
                if ($('.m-mobilemenu-shop').parent().hasClass('m-mobilemenu-shop-expanded')) {
                    $('.m-mobilemenu-content').css('height', initExpandedHeight);
                } else {
                    activeEl = $(el).children('.is-active').first();
                    $('.m-mobilemenu-content').css('height', initCollapsedHeight);
                }
            }
            $('.m-mobilemenu-content').scrollTop(0);  
        });
    }
}
