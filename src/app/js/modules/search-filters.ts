
// --
// Header Navigation
// --
//

// --
// USAGE
// --
//
// place usefull comments about usage here
// 

import $ = require('jquery');
import ElementAbstract from "src/app/js/system/ElementAbstract"

export default class SearchFilters extends ElementAbstract {
    private itemHeight = 54;
    private activeClass = 'is-active';
    private activeEl;

    constructor(el, options) {
        super(el, $.extend(true, {}, options), 'module');
        this.initialise(el);
    }


    initialise(el) {
        var scope = this;
        //this.$el.
        // console.log(this.$el);
        const sidebar = $(el);
        var activeEl = sidebar.children('.is-active').first();
        const dropdownSiblings = sidebar.find('.m-mobilemenu-content a');
        const levelClass = 'menu-level';
        const mobileHeaderHeight = 140;
        const initExpandedHeight = (activeEl.children("ul").first().children("li").length - 1 + activeEl.find(".m-mobilemenu-shop").children("li").length) * scope.itemHeight + 'px';
        const initCollapsedHeight = $('.m-mobilemenu-content ul').height() + 'px';
        console.log(initExpandedHeight);
        var firstTime = true;

        $('.js-toggle-filters').on('click', function (e) {
            e.preventDefault();
            $('body').toggleClass('js-mobile-filters-open');
        });

        /*$('.c-filter__link').on('click', function (e) {
            e.preventDefault();
            buildSearchUrl($(this));
        });

        $('.js-search-sort').on('change', function(e){
            var sortValue = $(this).val();
            var sortLabel = $('option:selected', this).text();
            $('.js-selected-sort').attr('data-selected-value', sortValue)
            $('.js-selected-sort').text(sortLabel);
        });

        function buildSearchUrl(el) {
            var url = "/categoryindexdisplay.do?q="
            $(".is-selected").each(function () {
                url += el.attr('data-facet-name') + "=" + el.attr('data-facet-value');
                window.location.href = url;
            });
        }*/

    }
}
