import Debouncer = require("./Debouncer");
import $ = require('jquery');

/**
 * 
 * == DebouncerWindowResize ==
 * 
 * A class which listens to the window being resized 
 * then calls the passed in callback after the specified debounce duration 
 * It also calls the passed in callback as soon as the user presses enter
 * 
 */

class DebouncerWindowResize extends Debouncer
{
    constructor(_callback:Function, _debounceTimeMilliseconds:number = undefined)
    {
        if(typeof _debounceTimeMilliseconds === "undefined")
        {
            _debounceTimeMilliseconds = 500;
        }

        super(_callback,_debounceTimeMilliseconds);
        this.initialise();
    }

    /**
     * 
     * Initialise
     * 
     */
    private initialise()
    {
        this.initialiseListeners();
    }


    /**
     * 
     * Initialise Listener for the keyup
     * 
     */
    private initialiseListeners()
    {
        $(window).resize(() => this.onWindowResized());
    }


    /**
     * 
     * When the window has been resized for any reason 
     * 
     */
    private onWindowResized()
    {
        this.reset();
    }

}

export = DebouncerWindowResize;