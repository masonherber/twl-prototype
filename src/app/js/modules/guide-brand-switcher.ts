

import $ = require('jquery');
import ElementAbstract from "../system/ElementAbstract"

export default class BrandSwitcher extends ElementAbstract
{

	private selectedState = "red";
	private cssPath = "/assets/css/";

	constructor(el, options)
	{
		super(el, $.extend(true, {
			htmlsource: null
		}, options), 'module');

		this.initialise();
		this.initialiseListeners();
	}

	initialise(){
    	$('body').addClass('-brand-t7');
	}

	private initialiseListeners(){
		$(this.$el).on('change',(e:JQueryEventObject) => this.onBrandChanged(e.originalEvent as MouseEvent));
	}

	private onBrandChanged(_mouseEvent:MouseEvent):void
    {
		_mouseEvent.preventDefault();
		var id = $(_mouseEvent.currentTarget).find("option:selected").attr("id")
		console.log("brand change", id);
		
		// $("#site").attr("href", this.cssPath + id + "-site.css"); 
		// $("#styleguide").attr("href", this.cssPath + id + "-styleguide.css"); 
		// $("#responsive").attr("href", this.cssPath + id + "-responsive.css"); 
		$("#main").attr("href", this.cssPath + "main-" + id + ".css"); 
		
		$('body').removeClass('-brand-red').removeClass('-brand-t7').removeClass('-brand-blue').addClass('-brand-' + id);
    }

}
