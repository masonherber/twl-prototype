
// --
// Featured Categories Module
// --
//
// Displays on the CLP page. Displays category cards and functionality includes resizing height on window resize and expand and collapse on click button

import $ = require('jquery');
import ElementAbstract from "src/app/js/system/ElementAbstract";
import DebouncerWindowResize = require("./debouncer-window-resize");

export default class CategoriesShowHide extends ElementAbstract
{
	
	private m_debounceWindowResize:DebouncerWindowResize = null; 
	private screenWidth:number = null;
	private breakpoint:number = null;
	private wrapHeight:number = null;
	private resizeFrom:number = 768;
	private expandBtn:JQuery = null;
	private revealWrap:JQuery = null;
	private firstCard:JQuery = null;
	private expanded:Boolean = false;
	private qty:number = 0;
	private rows:number = 0;

	constructor(el, options)
	{
		super(el, $.extend(true, {}, options), 'module');
		this.initialise();
	}

	initialise()
	{
		
		this.expandBtn = this.$el.find(".js-expand-btn");
		// console.log("this.expandBtn: ", this.expandBtn);
		this.revealWrap = this.$el.find(".js-reveal-wrap");
		this.qty = this.revealWrap.find(".u-columns__column").length;
		this.rows = Math.ceil(this.qty / 6);

		this.firstCard = this.$el.find(".js-card-category:first-child");

		this.initialiseExpandListeners();
		// console.log("this.revealWrap: ", this.revealWrap);

		this.m_debounceWindowResize = new DebouncerWindowResize(() => this.onWindowResizeCheckBreakPoints(),100);
		this.resizeContainer();
	}

	initialiseExpandListeners() {
		// check if less than 6 cards than will display as 1 row so disable button:
		if ( this.qty <= 6 ) {
			$(".js-expand-btn").hide();
			return;
		}

		this.expandBtn.click((e:any) => this.toggleCategoriesHeight(e));
	}

	private toggleCategoriesHeight(e) {
		// console.log("toggleCategoriesHeight");
		e.preventDefault();
        var scope = this;
        if(scope.revealWrap.hasClass('toggle-reveal')) {
			// console.log("hasClass toggle-reveal");
            scope.revealWrap.removeClass('toggle-reveal');
			scope.expandBtn.text("Show more");
			scope.resizeContainer();
        }
        else  {
			// console.log("! hasClass toggle-reveal");
			var wrapHeight = scope.firstCard.height() * scope.rows;
            // console.log("wrapHeight: ", wrapHeight);
            scope.revealWrap.height(wrapHeight);
            scope.revealWrap.delay( 500 ).addClass('toggle-reveal');
			scope.expandBtn.text("Show less");
        }
	}

	private onWindowResizeCheckBreakPoints()
    {
		// console.log("onWindowResizeSettled");	
		var windowSize = $(document).width();
			// console.log("screenWidth: ", this.screenWidth);

		if(this.breakpoint !== null && this.breakpoint === windowSize)
			{
			//No point in resizing the images, as they're based on the width of the browser
			//it may have been vertically resized, like scrolling on an ipad and the tab bar disappears
			return;
		}
		
		this.breakpoint = windowSize;

        this.resizeContainer();
	}

	private getCurrentScreenWidth():number {
		this.screenWidth = $(document).width();
		// console.log("this.screenWidth: ", this.screenWidth);
		return this.screenWidth;
	}

	private removeHeight() {
		this.revealWrap.removeAttr("style");
	}

	private resizeContainer() {

		// if(this.expanded) {
		// 	console.log("already expanded do nothing");
		// 	return;
		// }
		this.getCurrentScreenWidth();

		// console.log("resizeContainer");
		var wrapHeight = this.$el.height();

		if(this.screenWidth >= this.resizeFrom) {
			if(this.wrapHeight !== null && this.wrapHeight === wrapHeight || this.expanded)
				{
					console.log("if condition");
				//it may have been vertically resized, like scrolling on an ipad and the tab bar disappears
				return;
			}

			// console.log("screenWidth > resizeFrom so resizeHeight");
			this.wrapHeight = this.firstCard.height();
			// console.log("this.wrapHeight: ", this.wrapHeight);
			this.revealWrap.height(this.wrapHeight);
			this.revealWrap.removeClass('toggle-reveal');
			this.expandBtn.text("Show more");
		} else {
			// console.log("screenWidth < resizeFrom so removeStyle");
			this.revealWrap.removeAttr("style");
		}
		
		
	}

}
