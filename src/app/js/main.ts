// import main loader
import './system/InlineClassLoader'

// load app
import App = require("./system/app");
var app = new App({});