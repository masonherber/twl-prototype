if (typeof (Twg) === "undefined") {
	Twg = {};
}

if (typeof (Twg.Modules) === "undefined") {
	Twg.Modules = {};
}

if (typeof (Twg.Modules.Event) === "undefined") {
	Twg.Modules.Event = {};
}

if (typeof (Twg.Modules.Modernizer) === "undefined") {
	Twg.Modules.Modernizer = {};
}

if (typeof (Twg.Modules.Page) === "undefined") {
	Twg.Modules.Page = {};
}

if (typeof (Twg.Modules.Component) === "undefined") {
	Twg.Modules.Component = {};
}

if(typeof (Twg.Modules.Common) === "undefined"){
	Twg.Modules.Common = { };
}

if (typeof (Twg.Modules.Ajax) === "undefined") {
	Twg.Modules.Ajax = {};
}

if (typeof (Twg.Modules.Ajax.Partial) === "undefined") {
	Twg.Modules.Ajax.Partial = {};
}

if (typeof (Twg.Modules.Resources) === "undefined") {
	Twg.Modules.Resources = {};
}

if (typeof (Twg.Modules.Services) === "undefined") {
	Twg.Modules.Services = {};
}

