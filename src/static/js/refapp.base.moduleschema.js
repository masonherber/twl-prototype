if (typeof (app) === "undefined") {
    app = {};
}

if (typeof (RefApp) === "undefined") {
	RefApp = {};
}

if (typeof (RefApp.Modules) === "undefined") {
	RefApp.Modules = {};
}

if (typeof (RefApp.Modules.Event) === "undefined") {
	RefApp.Modules.Event = {};
}

if (typeof (RefApp.Modules.Modernizer) === "undefined") {
	RefApp.Modules.Modernizer = {};
}

if (typeof (RefApp.Modules.Page) === "undefined") {
	RefApp.Modules.Page = {};
}

if (typeof (RefApp.Modules.Component) === "undefined") {
	RefApp.Modules.Component = {};
}

if(typeof (RefApp.Modules.Common) === "undefined"){
	RefApp.Modules.Common = { };
}

if (typeof (RefApp.Modules.Ajax) === "undefined") {
	RefApp.Modules.Ajax = {};
}

if (typeof (RefApp.Modules.Ajax.Partial) === "undefined") {
	RefApp.Modules.Ajax.Partial = {};
}

if (typeof (RefApp.Modules.Resources) === "undefined") {
	RefApp.Modules.Resources = {};
}

if (typeof (RefApp.Modules.Services) === "undefined") {
	RefApp.Modules.Services = {};
}

