RefApp.Modules.Page.Styleguide = (function ($) {
    "use strict";
    var HEX_DIGITS = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f");
    // Update the colourCount value below
    var COLOUR_COUNT = 10;

    var instance = {};

    instance.enableJavaScriptComponents = function (colourCount) {
        if (colourCount) {
            COLOUR_COUNT = colourCount;
        }
        addColourPaletteItems();
        addColourValuesToColourPaletteItems();
        removeNoJavaScriptMessages();
    };

    var addColourPaletteItems = function () {
        for (var index = 1; index <= COLOUR_COUNT; index++) {
            var leadingZero = index < 10 ? "0" : "";
            var colourClass = "sg-swatch" + leadingZero + index;
            var colourVariable = "@color" + leadingZero + index;

            $("#colour-palette ul").append("<li><div class='" + colourClass + "'></div>" +
                "<code>" + colourVariable + "</code><span></span></li>");
        }
    };

    var removeNoJavaScriptMessages = function () {
        $(".no-js").remove();
    };

    var addColourValuesToColourPaletteItems = function () {
        $("#colour-palette li").each(function () {
            var $swatchContainer = $(this);
            var paletteColour = $swatchContainer.children("div").css("background-color");
            var colorHexValue = rgb2hex(paletteColour);
            $swatchContainer.children("span").html(colorHexValue);
        });
    };

    var rgb2hex = function (rgb) {
        try {
            if (rgb === "undefined") {
                return "unknown";
            }
            else if (rgb.indexOf("#") === 0) {
                // IE8 already provides HEX value
                return rgb;
            }
            else {
                rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
                return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
            }
        }
        catch (err) {
            return "unknown";
        }
    };

    var hex = function (base10Number) {
        return isNaN(base10Number) ? "00" : HEX_DIGITS[(base10Number - base10Number % 16) / 16] + HEX_DIGITS[base10Number % 16];
    };

    instance.docifyExamples = function () {
        $(".sg-example").each(function () {
            var $sgComponent = $(this);
            if ($sgComponent.hasClass("sg-show-dom")) {
                $sgComponent.after("<div class='sg-markup'></div>");
                var markupObject = $sgComponent.html();
                var lines = markupObject.split("\n");
                var indentLength = 0;
                var indentDetermined = false;
                var formattedLines = [];
                for (var i = 0; i < lines.length; i++) {
                    var currentLine = lines[i];
                    if (currentLine.length != 0) {
                        if (!indentDetermined) {
                            var currentLineTrimmed = $.trim(currentLine);
                            indentLength = currentLine.indexOf(currentLineTrimmed);
                            indentDetermined = true;
                        }
                        formattedLines.push(currentLine.substr(indentLength));
                    }
                }
                var formattedMarkup = formattedLines.join("\n");
                $sgComponent.next($('.sg-markup')).text(formattedMarkup);
            }
        });
        $('.sg-markup').wrapInner('<pre class="prettyprint linenums" />');
        $('.sg-markup').prepend("<div class='sg-markup-toggle'><a href='#'>Hide/Show Code</a></div>");
        window.prettyPrint && prettyPrint();
        attachToggleLinkEvents();
    };

    var attachToggleLinkEvents = function() {
        $(".sg-markup-toggle a").on("click", function(e) {
            e.preventDefault();
            var $toggleLink = $(this);
            $toggleLink.closest(".sg-markup").toggleClass("sg-expanded");
        });
    };

    instance.loadMaskExample = function () {
        app.progress.show(".sg-loading-mask-example");
    };

    instance.destroySampleTooltips = function () {
        $(".sg-tooltip").tooltip("destroy");
    };

    return instance;

} (window.jQuery));