/*
 * This file sets the global variables for all js files
 */

(function( amblique, $, undefined ) {   
    
    //Public Method for the responsive design javascript
	//Declare all variables that are located within the responsive design 
    amblique.responsive = function() {
    	amblique.responsive.menuHeight = 0;    
    };
    
    amblique.responsive();
        
}( window.amblique = window.amblique || {}, jQuery ));

