(function($,sr){

  // debouncing function from John Hann
  // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
    var debounce = function (func, threshold, execAsap) {
    var timeout;

    return function debounced () {
        var obj = this, args = arguments;
        function delayed () {
            if (!execAsap)
                func.apply(obj, args);
                timeout = null;
        };

        if (timeout)
            clearTimeout(timeout);
        else if (execAsap)
            func.apply(obj, args);

        timeout = setTimeout(delayed, threshold || 500);
    };
  }
    // smartresize
    jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

}(jQuery,'smartresize'));


/*
 * All java script logic for the mobile layout.
 *
 * The code relies on the jQuery JS library to be also loaded.
 *
 * The logic extends the JS namespace app.*
 */
(function(app, $, undefined) {
    app.responsive = {
        mobileLayoutWidth : 760,
        tabletLayoutWidth : 1024,

        init : function () {

            $cache = {
                    wrapper: $('#wrapper'),
                    navigation: $('#navigation'),
                    homepageSlider: $('#homepage-slider'),
                    window: $(window)
                };

            // toggle menu element
            $cache.navigation.find('.navigation-header')
                .click(function(){
                    jQuery(this).siblings('.menu-category').toggle();
                });

            //handle click on responsive-mode
            $cache.navigation.find('li.category-level-2,.menu-category>li')
            .on('click',function(e){
                //check if in mobile mode
                if(jQuery(this).parents('.nav-stacked').length != 0) {
                    //child tag <span> click handler
                    if(jQuery(e.target).closest('span').length > 0 || $(e.target).hasClass('link-headers')) {
                            e.preventDefault();
                            if(jQuery(this).hasClass('level-1') || jQuery(this).children('a').hasClass('level-1')) {
                                jQuery(this).find('span').first().toggleClass('icon-mobile-arrow-up icon-mobile-arrow-down');
                                jQuery(this).children().siblings('div').toggleClass('onHide');
                            } else {
                                jQuery(this).find('span').first().toggleClass('icon-mobile-plus icon-mobile-minus');
                                jQuery(this).children().siblings('.navbar-items').toggle();
                            }

                        } else {
                            // parent tag <a> click handler
                            if(jQuery(this).children('ul').is(':hidden')) {
                                e.preventDefault();
                                jQuery(this).find('span').first().toggleClass('icon-mobile-plus icon-mobile-minus');
                                jQuery(this).children().siblings('.navbar-items').toggle();
                            } else if($(e.target).closest('a').length == 0 || $(this).find('.menu-navigation').hasClass('onHide')) {
                                jQuery(this).children().siblings('div').toggleClass('onHide');
                                jQuery(this).find('span').first().toggleClass('icon-mobile-arrow-up icon-mobile-arrow-down');
                            } else {
                            return true;
                        }
                    }
                    return false;
                } else {
                    return true;
                }
            });

            //add onHide Class when ready
            $cache.navigation.find('.menu-navigation').addClass('onHide');
        },

        isMobileLayout : function () {
            return $cache.window.width() < this.mobileLayoutWidth;
        },

        isTabletLayout : function () {
            return ($cache.wrapper.width() > this.mobileLayoutWidth) && ($cache.wrapper.width() <= this.tabletLayoutWidth);
        },

        enableMobileComponents : function ()
        {
            //app.responsive.enableMobileNav(); //amblique's function
            app.responsive.enableMobileMiniMenu();
            app.responsive.disableSwiperPagination();
            //my responsive navbar enable handler
            //app.responsive.enableMobileNavBar();
        },

        disableMobileComponents : function ()
        {
            //app.responsive.disableMobileNav();
            app.responsive.disableMobileMiniMenu();
            app.responsive.enableSwiperPagination();
        },

        enableMobileNavBar : function(){
            if(!$cache.navigation.find('.menu-category').hasClass('nav-stacked')){
                $cache.navigation.find('.menu-category').addClass('nav-stacked');
                $cache.navigation.find('.navbar-items').css({'display':'none'});
                $cache.navigation.find('.menu-category').hide()
            }

        },


        // build vertical, collapsable menu
        enableMobileNav : function(){
            $cache.navigation.find('.menu-category')
                .hide()
                .addClass('nav-stacked')
                .children('li')
                    .children('a')
                        .click(function(){
                            if( (jQuery(this).siblings().length > 0 ) && (!jQuery(this).siblings().is(":visible"))) {
                                jQuery(this)
                                    .append('<span>close</span>')
                                    .children('span')
                                        .click(function(){
                                            jQuery(this).parent().siblings().hide();
                                            jQuery(this).remove();
                                            return false;
                                        })
                                    .parent().siblings().show();
                                return false;
                            }
                        })
        },

        // revert to standard horizontal menu
        disableMobileNav : function(){
            $cache.navigation.find('.menu-category').show();
            $cache.navigation.find('.menu-category').removeClass('nav-stacked');
            $cache.navigation.find('.level-2').removeAttr('style');
            //cache.navigation.find('.level-1 span').remove();
            $cache.navigation.find('.navbar-items').removeAttr('style');
            $cache.navigation.find('.menu-navigation').addClass('onHide');
        },

        disableSwiperPagination : function() {
            $(".swiper-container").each(function() {
                var $swiperComponent = $(this);
                var swiperControl = $swiperComponent.data("swiper");
                if (typeof (swiperControl) !== "undefined" &&
                        swiperControl.params.pagination &&
                        swiperControl.params.paginationClickable &&
                        swiperControl.params.disablePaginationOnMobile) {
                    swiperControl.params.paginationClickable = false;
                    swiperControl.createPagination();
                }
            });
        },

        enableSwiperPagination : function() {
            $(".swiper-container").each(function() {
                var $swiperComponent = $(this);
                var swiperControl = $swiperComponent.data("swiper");
                if (typeof (swiperControl) !== "undefined" &&
                        swiperControl.params.pagination &&
                        !swiperControl.params.paginationClickable &&
                        swiperControl.params.disablePaginationOnMobile) {
                    swiperControl.params.paginationClickable = true;
                    swiperControl.createPagination();
                }
            });
        },

        toggleGridWideTileView : function(){

            /*    toggle grid/wide tile    */
            if(jQuery('.toggle-grid').length == 0)
            {
                /* On clickthrough and FSD don't have it, so I comment it.
                jQuery('.results-hits').prepend('<a class="toggle-grid" href="'+location.href+'">+</a>');
                jQuery('.toggle-grid').click(function(){
                    jQuery('.search-result-content').toggleClass('wide-tiles');
                    return false;
                });
                */
            }
        },

        enableMobileMiniMenu : function() {
            $('.header-menu').removeClass('menu-utility');
            $('.header-menu').addClass('mobile-mini-menu-content');
            $('.header-menu').hide();
        },

        disableMobileMiniMenu : function() {
            $('.header-menu').addClass('menu-utility');
            $('.header-menu').removeClass('mobile-mini-menu-content');
            $('.header-menu').hide();
        },

        updateSwiperControls : function() {
            $(".swiper-container").each(function() {
                var $swiperComponent = $(this);
                var swiperControl = $swiperComponent.data("swiper");
                if (swiperControl === undefined) {
                    return;
                }
                if (swiperControl.params.responsiveReinit) {
                    swiperControl.reInit(true);
                    swiperControl.params.onlyExternal = swiperControl.slides.length == swiperControl.visibleSlides.length;
                    swiperControl.params.slidesPerGroup = swiperControl.visibleSlides.length;
                }
                if ($swiperComponent.hasClass("product-swiper") && $swiperComponent.closest(".hidden-phone").length == 0) {
                    if (app.responsive.isMobileLayout()) {
                        swiperControl.params.slidesPerView = 2;
                        swiperControl.params.slidesPerGroup = 2;
                    }
                    else {
                        swiperControl.params.slidesPerView = $swiperComponent.data("slidesperview");
                        swiperControl.params.slidesPerGroup = $swiperComponent.data("slidespergroup");
                    }
                    swiperControl.reInit(true);
                }
            });
        }
    }

    $(document).ready(function(){
        app.responsive.init();
        var _wasMobileLayout = app.responsive.isMobileLayout();

        $(window).smartresize(function(){
            var isMobileLayout = app.responsive.isMobileLayout();
            if (_wasMobileLayout && !isMobileLayout) {
                // Do Small Desktop/ Large Desktop things
                _wasMobileLayout = false;
                app.responsive.updateSwiperControls();
            } else if (!_wasMobileLayout && isMobileLayout) {
                // Do Mobile things
                _wasMobileLayout = true;
                app.responsive.updateSwiperControls();
            }
        });

        InitMobileMiniMenu();
    });

}(window.app = window.app || {}, jQuery));


function InitMobileMiniMenu() {
    $('.mobile-mini-menu').toggle(function() {
        $('.mobile-mini-menu').addClass('expanded');
        $('.header-menu').addClass('mobile-mini-menu-content');
        $('.header-menu').slideDown('slow', function() {
        // Animation complete.
        });
        $('#header').animate({
            height: '+='+amblique.responsive.menuHeight,
            }, 458, 'swing', function() {
        });
    }, function() {
        $('.mobile-mini-menu').removeClass('expanded');
        $('.header-menu').slideUp('slow', function() {
        });

        $('#header').animate({
            height: '-='+amblique.responsive.menuHeight,
        }, 458, 'swing', function() {
        });
    });
};
