if (typeof (Twl) === "undefined") {
	Twl = {};
}

if (typeof (Twl.Modules) === "undefined") {
	Twl.Modules = {};
}

if (typeof (Twl.Modules.Event) === "undefined") {
	Twl.Modules.Event = {};
}

if (typeof (Twl.Modules.Modernizer) === "undefined") {
	Twl.Modules.Modernizer = {};
}

if (typeof (Twl.Modules.Page) === "undefined") {
	Twl.Modules.Page = {};
}

if (typeof (Twl.Modules.Component) === "undefined") {
	Twl.Modules.Component = {};
}

if(typeof (Twl.Modules.Common) === "undefined"){
	Twl.Modules.Common = { };
}

if (typeof (Twl.Modules.Ajax) === "undefined") {
	Twl.Modules.Ajax = {};
}

if (typeof (Twl.Modules.Ajax.Partial) === "undefined") {
	Twl.Modules.Ajax.Partial = {};
}

if (typeof (Twl.Modules.Resources) === "undefined") {
	Twl.Modules.Resources = {};
}

if (typeof (Twl.Modules.Services) === "undefined") {
	Twl.Modules.Services = {};
}
