# README #

### What is this repository for? ###

* A Repo for the pattern library and prototype framework for The Warehouse Group
* Version 0.1

### How do I get set up? ###

* Install NVM
* $ cd Frontend
* $ vnm use
* $ sudo npm install gulp -g
* $ sudo npm install

### Gulp Tasks ###

* $ gulp (default) - builds prototype for local testing
* $ gulp production - builds and deploys to public website directory structure

### Tech Stack ###

* NVM Node Version Manager
* NPM
* Gulp
* SVG Sprites
* SCSS Approach:
* ITCSS (For optimium structuring the SCSS files)
* BEM (SCSS Naming scheme)
* Webpack (JavaScript bundling)
* ES6 / Typescript
* Numjucks (HTML Templating)

