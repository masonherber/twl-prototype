/**
 * 
 * Normal development Usage:
 * 	-Use "gulp" in terminal / console
 * 	-This will create a local server (generally http://localhost:3000, port is dependant on if there's another process taking up port 3000)
 *  -Source maps will be created to allow for easy JavaScript debugging in the browser and in visual studio code
 *  -JavaScript AND CSS is not minified, this is both for debugging and for speed of compilation while developing
 *  -Watches for any changes in css / html / js and triggers tha appropritae action (compile or copy etc) then refreshes the browser
 * 
 * For production use:
 *  -In conjunction to development mode (open another terminal) you can run "gulp production" (so you can run both development and production tasks if you want)
 *  -This will not create a local server, it is purely for exporting files for production in the CMS folder
 *  -This will remove any source maps (as you dont want to publically show debugging information)
 *  -Minification of JavaScript and CSS files, these are all bundled into one .js and .css file respectively
 *  -Watches for any changes in css / html / js and triggers the appropritae action (compile or copy etc), DOES NOT refresh the browser
 * 
 * 
 */


// 
// -------------------------------------
//   IMPORTS
// -------------------------------------
//  

var fs = require('fs');
var packageSettings = JSON.parse(fs.readFileSync('./package.json'));

var gulp           = require('gulp'),
	browserSync    = require('browser-sync'),
    filters        = require('gulp-filter'),
	sass           = require('gulp-sass'),
	gulpif           = require('gulp-if'),
	sourcemaps     = require('gulp-sourcemaps'),
	notify 		   = require("gulp-notify"),
	autoprefixer   = require('gulp-autoprefixer'),
	cssnano 	   = require('gulp-cssnano'),
	rename 		   = require('gulp-rename'),
    del 		   = require('del'),
    runSequence    = require('run-sequence'),
    //for server:
	gutil    	   = require('gulp-util'),
	compress 	   = require('compression'),
	logger   	   = require('morgan'),
	open     	   = require('open'),
	//for webpack
	webpack        = require('webpack-stream'),
	webpackConfig  = require('./webpack.config.js'),
	//for nunjucks
	nunjucksRender = require('gulp-nunjucks-render'),
	chokidar = require('chokidar'),
	svgSprite = require('gulp-svg-sprite'),
	sftp = require('gulp-sftp');

const bProductionMode = process.argv.indexOf("production") !== -1;

// 
// -------------------------------------
//   CONFIG
// -------------------------------------
// 

var config                  = {}

//
// CORE PATHS
// 

config.buildDirectory       = (bProductionMode) ? "./dist/" : "./build/"
//config.publicRoot  			= "./CMS/"
config.publicRoot  			= (bProductionMode) ? "./dist/" : "./build/"


// config.publicRoot  			= "./CMS/"
config.sourceDirectory      = "./src"

config.sourceAssets         = config.sourceDirectory + '/assets'
config.sourceApp            = config.sourceDirectory + '/app'
config.sourceCore           = config.sourceApp + '/core'

config.buildAssets          = config.buildDirectory + '/assets'
config.buildHTML       		= config.buildDirectory + '/html'

config.defaultPagePath      = "/html/"

//
// -------------------------------------
// WEBPACK
// -------------------------------------
//

//
// CONFIG
//

config.webpack_src           = config.sourceApp + "/js/main.js"
config.webpack_dest          = config.buildDirectory + "/assets/js"
config.webpack_watch         = config.sourceApp + "/**/*.ts"

config.scripts_src           = config.sourceApp + "/js/custom/**/*.js"
config.scripts_dest          = config.buildDirectory + "/assets/js/custom"
config.scripts_watch         = config.scripts_src

config.svg_src = config.sourceDirectory + "/assets/icons/svg-sprite/";
config.svg_dest = config.buildAssets;
config.svg_config = {
	mode: {
		defs: {
			dest: '../../' + config.buildAssets, // Keep the intermediate files
			prefix: "", // Prefix for CSS selectors
		}
	}
};



gulp.task('svg-sprite', function () {
	return gulp.src('**/*.svg', {
		cwd: config.svg_src
	})
		.pipe(svgSprite(config.svg_config))
		.on('error', handleErrors)
		.pipe(gulp.dest(config.svg_dest));
});


//
// TASK
//

gulp.task('webpack', function() {
	console.log("webpack Task");
  return gulp.src(config.webpack_src)
    .pipe(webpack(webpackConfig))
	.on('error', handleErrors)
    .pipe(gulp.dest(config.webpack_dest))
	
	.pipe(gulpif(!bProductionMode,browserSync.reload(
		{
		stream: true
    	}
	)));
});

gulp.task('scripts', function() {
	return gulp.src([config.scripts_src])
		.pipe(gulp.dest(config.scripts_dest));//publish to build folder
});




//
// -------------------------------------
// STYLES
// -------------------------------------
//

//
// CONFIG
//
config.scss_folder          = config.sourceApp + "/scss/";
config.scss_src             = config.scss_folder + "*.scss";

config.scss_core_src    	= config.scss_folder + "main-core.scss";
config.scss_t7_min_src    		= config.scss_folder + "main-t7-min.scss";
config.scss_t7_src    		= config.scss_folder + "main-t7.scss";

config.scss_src_core        = config.sourceCore + "/scss";
config.css_dest             = config.buildAssets + '/css';
config.scss_watch           = config.sourceApp + "/**/*.scss";

config.scss_settings = { indentedSyntax: false, outputStyle: 'compressed'  }
config.scss_settings_dev = { indentedSyntax: true, outputStyle: 'expanded' }
config.autoprefixer         = { browsers: ['ie >= 9','iOS 7']}

//
// Blue and red config
//
config.scss_blue_src    		= config.scss_folder + "main-blue.scss";
config.scss_red_src         = config.scss_folder + "main-red.scss";
config.scss_red_live_static_src    = config.scss_folder + "main-red-live-static.scss";
config.scss_red_live_guide_src    = config.scss_folder + "main-red-live-guide.scss";
config.scss_red_live_src    = config.scss_folder + "main-red-live.scss";



gulp.task('styles-t7', function() 
{
	return gulp.src([config.scss_t7_min_src])
		// .pipe(filter)
		.pipe(sass(config.scss_settings))
		.on('error', handleErrors)
		.pipe(autoprefixer(config.autoprefixer))
		//.pipe(sass({outputStyle: 'compressed'}))
		//.pipe(sass({outputStyle: 'expanded'}))
		// .pipe(rename({suffix: '.min'}))
		.pipe(gulpif(bProductionMode, cssnano({autoprefixer:false}))) //Only if in production mode run this
		.pipe(gulp.dest(config.css_dest))//publish to build folder
		.pipe(gulpif(!bProductionMode,notify({ message: 'Styles task complete' })))
		.pipe(gulpif(!bProductionMode, browserSync.stream()));
});

gulp.task('styles-t7-guide', function () {
	return gulp.src([config.scss_t7_src])
		// .pipe(filter)
		.pipe(sourcemaps.init())
		.pipe(sass(config.scss_settings_dev))
		.on('error', handleErrors)
		.pipe(autoprefixer(config.autoprefixer))
		//.pipe(sass({ outputStyle: 'compressed' }))
		//.pipe(sass({outputStyle: 'expanded'}))
		// .pipe(rename({suffix: '.min'}))
		.pipe(gulpif(bProductionMode, cssnano({ autoprefixer: false }))) //Only if in production mode run this
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(config.css_dest))//publish to build folder
		.pipe(gulpif(!bProductionMode, notify({ message: 'Styles task complete' })))
		.pipe(gulpif(!bProductionMode, browserSync.stream()));
});

//
// Blue and red Styles tasks
//

gulp.task('styles-red', function() 
{
	return gulp.src([config.scss_red_src])
		// .pipe(filter)
		.pipe(sourcemaps.init())
		.pipe(sass(config.scss_settings))
		.on('error', handleErrors)
		.pipe(autoprefixer(config.autoprefixer))
		// .pipe(sass({outputStyle: 'compressed'}))
		//.pipe(sass({outputStyle: 'expanded'}))
		// .pipe(rename({suffix: '.min'}))
		.pipe(gulpif(bProductionMode, cssnano({autoprefixer:false}))) //Only if in production mode run this
		.pipe(gulp.dest(config.css_dest))//publish to build folder
		.pipe(gulpif(!bProductionMode,notify({ message: 'Styles task complete' })))
		.pipe(gulpif(!bProductionMode, browserSync.stream()));
});

gulp.task('styles-blue', function() 
{
	return gulp.src([config.scss_blue_src])
		// .pipe(filter)
		.pipe(sourcemaps.init())
		.pipe(sass(config.scss_settings))
		.on('error', handleErrors)
		.pipe(autoprefixer(config.autoprefixer))
		// .pipe(sass({outputStyle: 'compressed'}))
		//.pipe(sass({outputStyle: 'expanded'}))
		// .pipe(rename({suffix: '.min'}))
		.pipe(gulpif(bProductionMode, cssnano({autoprefixer:false}))) //Only if in production mode run this
		.pipe(gulp.dest(config.css_dest))//publish to build folder
		.pipe(gulpif(!bProductionMode,notify({ message: 'Styles task complete' })))
		.pipe(gulpif(!bProductionMode, browserSync.stream()));
});

gulp.task('styles-red-live-static', function() 
{
	return gulp.src([config.scss_red_live_static_src])
		// .pipe(filter)
		
		.pipe(sass(config.scss_settings))
		.on('error', handleErrors)
		// .pipe(autoprefixer(config.autoprefixer))
		// .pipe(sass({outputStyle: 'compressed'}))
		//.pipe(sass({outputStyle: 'expanded'}))
		// .pipe(rename({suffix: '.min'}))
		.pipe(gulpif(bProductionMode, cssnano({autoprefixer:false}))) //Only if in production mode run this
		.pipe(gulp.dest(config.css_dest))//publish to build folder
		.pipe(gulpif(!bProductionMode,notify({ message: 'Styles task complete' })))
		.pipe(gulpif(!bProductionMode, browserSync.stream()));
});

gulp.task('styles-red-live-guide', function() 
{
	return gulp.src([config.scss_red_live_guide_src])
		// .pipe(filter)
		.pipe(sourcemaps.init())
		.pipe(sass(config.scss_settings))
		.on('error', handleErrors)
		.pipe(autoprefixer(config.autoprefixer))
		.pipe(sass())
		//.pipe(sass({outputStyle: 'expanded'}))
		// .pipe(rename({suffix: '.min'}))
		.pipe(gulpif(bProductionMode, cssnano({autoprefixer:false}))) //Only if in production mode run this
		.pipe(gulp.dest(config.css_dest))//publish to build folder
		.pipe(gulpif(!bProductionMode,notify({ message: 'Styles task complete' })))
		.pipe(gulpif(!bProductionMode, browserSync.stream()));
});

gulp.task('styles-red-live', function() 
{
	return gulp.src([config.scss_red_live_src])
		// .pipe(filter)
		.pipe(sourcemaps.init())
		.pipe(sass(config.scss_settings))
		.on('error', handleErrors)
		.pipe(autoprefixer(config.autoprefixer))
		.pipe(sass())
		//.pipe(sass({outputStyle: 'expanded'}))
		// .pipe(rename({suffix: '.min'}))
		.pipe(gulpif(bProductionMode, cssnano({autoprefixer:false}))) //Only if in production mode run this
		.pipe(gulp.dest(config.css_dest))//publish to build folder
		.pipe(gulpif(!bProductionMode,notify({ message: 'Styles task complete' })))
		.pipe(gulpif(!bProductionMode, browserSync.stream()));
});

// -------------------------------------
// HTML
// -------------------------------------

//
// CONFIG
//

config.html_src             = config.sourceApp + "/**/*.html"
config.html_dest            = config.buildDirectory
config.html_templates       = config.sourceApp
config.html_watch           = config.html_src

//
// TASK
//

gulp.task('html', function() {
  
	return gulp.src([config.sourceApp+'/**/*.html','!**/{templates,macros,core}/**'])
		.pipe(nunjucksRender
			(
				{
				path: [config.html_templates]
				}
			)
		)
		.on('error', handleErrors)
		.pipe(gulp.dest(config.html_dest))
		.pipe(gulpif(!bProductionMode, browserSync.stream()));
});



gulp.task('redirect-html', function() {
  
	return gulp.src([config.sourceDirectory+'/index.html'])
		.on('error', handleErrors)
		.pipe(gulp.dest("./build"));
});

gulp.task('redirect-html-dist', function () {

	return gulp.src([config.sourceDirectory + '/index.html'])
		.on('error', handleErrors)
		.pipe(gulp.dest("./dist"));
});

// -------------------------------------
// IMAGES
// -------------------------------------

//
// CONFIG
//

config.videos_src           = config.sourceAssets + "/videos/**"
config.videos_dest          = config.buildAssets + '/videos'
config.videos_watch         = config.sourceAssets + "/videos/**"

config.images_src           = config.sourceAssets + "/images/**"
config.images_dest          = config.buildAssets + '/images'
config.images_watch         = config.sourceAssets + "/images/**"

//
// TASK
//

gulp.task('images', function() 
{
	return gulp.src([config.images_src])
		.pipe(gulp.dest(config.images_dest))//publish to build folder
		.on('error', handleErrors)
		.pipe(gulpif(!bProductionMode, browserSync.stream()));
});


gulp.task('videos', function() 
{
	return gulp.src([config.videos_src])
		.pipe(gulp.dest(config.videos_dest))//publish to build folder
		.on('error', handleErrors)
		.pipe(gulpif(!bProductionMode, browserSync.stream()));
});


// -------------------------------------
// FONTS
// -------------------------------------

//
// CONFIG
//

config.fonts_src           = config.sourceAssets + "/fonts/**"
config.fonts_dest          = config.buildAssets + '/fonts'
config.fonts_watch         = config.sourceAssets + "/fonts/**"

// 
// TASK
//

gulp.task('fonts', function() 
{
	return gulp.src([config.fonts_src])
		.pipe(gulp.dest(config.fonts_dest))
		.on('error', handleErrors)
		.pipe(gulpif(!bProductionMode, browserSync.stream()));
});


// -------------------------------------
// BROWSER SYNC
// -------------------------------------

// 
// TASK
//

gulp.task('browserSync', function() {
	return browserSync( 
	{
		ghostMode: {
			clicks: true,
			forms: true,
			scroll: false // Causes issues with development having scroll enabled
		},
		notify: false,
		// files  : ['public/**/*.html'], //Don't need this as we call browsersync when nunjucks has rendered
		files: false,
		server : 
		{
			baseDir : config.publicRoot
		},
		startPath: config.defaultPagePath
	})
});




// -------------------------------------
// WATCH
// -------------------------------------

// 
// TASK
//

gulp.task('watch', function() 
{
	var watching_styles = chokidar.watch(config.scss_watch, {ignored: /[\/\\]\./, persistent: true});
	var watching_html = chokidar.watch(config.html_watch, {ignored: /[\/\\]\./, persistent: true});
	var watching_images = chokidar.watch(config.images_watch, {ignored: /[\/\\]\./, persistent: true});
	var watching_fonts = chokidar.watch(config.fonts_watch, {ignored: /[\/\\]\./, persistent: true});
	var watching_videos = chokidar.watch(config.videos_watch, {ignored: /[\/\\]\./, persistent: true});
	var watching_scripts = chokidar.watch(config.webpack_watch, {ignored: /[\/\\]\./, persistent: true});

	watching_styles.on('ready', (event, path) => {
		watching_styles.on('error', error => log(`Watcher error: ${error}`));
		watching_styles.on('all', (event, path) => {
			runSequence('styles-red', 'styles-blue', 'styles-red-live');
		});
	});

	watching_html.on('ready', (event, path) => {
		watching_html.on('error', error => log(`Watcher error: ${error}`));
		watching_html.on('all', (event, path) => {
			runSequence('html');
		});
	});

	watching_images.on('ready', (event, path) => {
		watching_images.on('error', error => log(`Watcher error: ${error}`));
		watching_images.on('all', (event, path) => {
			runSequence('images');
		});
	});

	watching_fonts.on('ready', (event, path) => {
		watching_fonts.on('error', error => log(`Watcher error: ${error}`));
		watching_fonts.on('all', (event, path) => {
			runSequence('fonts');
		});
	});

	watching_videos.on('ready', (event, path) => {
		watching_videos.on('error', error => log(`Watcher error: ${error}`));
		watching_videos.on('all', (event, path) => {
			runSequence('videos');
		});
	});

	watching_scripts.on('ready', (event, path) => {
		watching_scripts.on('error', error => log(`Watcher error: ${error}`));
		watching_scripts.on('all', (event, path) => {
			runSequence('webpack');
		});
	});

});

// 
// TASK watch for Red and blue
//

gulp.task('watchT7', function() 
{
	var watching_styles = chokidar.watch(config.scss_watch, {ignored: /[\/\\]\./, persistent: true});
	var watching_html = chokidar.watch(config.html_watch, {ignored: /[\/\\]\./, persistent: true});
	var watching_images = chokidar.watch(config.images_watch, {ignored: /[\/\\]\./, persistent: true});
	var watching_fonts = chokidar.watch(config.fonts_watch, {ignored: /[\/\\]\./, persistent: true});
	var watching_videos = chokidar.watch(config.videos_watch, {ignored: /[\/\\]\./, persistent: true});
	var watching_scripts = chokidar.watch(config.webpack_watch, {ignored: /[\/\\]\./, persistent: true});

	watching_styles.on('ready', (event, path) => {
		watching_styles.on('error', error => log(`Watcher error: ${error}`));
		watching_styles.on('all', (event, path) => {
			runSequence('styles-t7', 'styles-t7-guide');
		});
	});

	watching_html.on('ready', (event, path) => {
		watching_html.on('error', error => log(`Watcher error: ${error}`));
		watching_html.on('all', (event, path) => {
			runSequence('html');
		});
	});

	watching_images.on('ready', (event, path) => {
		watching_images.on('error', error => log(`Watcher error: ${error}`));
		watching_images.on('all', (event, path) => {
			runSequence('images');
		});
	});

	watching_fonts.on('ready', (event, path) => {
		watching_fonts.on('error', error => log(`Watcher error: ${error}`));
		watching_fonts.on('all', (event, path) => {
			runSequence('fonts');
		});
	});

	watching_videos.on('ready', (event, path) => {
		watching_videos.on('error', error => log(`Watcher error: ${error}`));
		watching_videos.on('all', (event, path) => {
			runSequence('videos');
		});
	});

	watching_scripts.on('ready', (event, path) => {
		watching_scripts.on('error', error => log(`Watcher error: ${error}`));
		watching_scripts.on('all', (event, path) => {
			runSequence('webpack');
		});
	});

});


// -------------------------------------
// CLEAN
// -------------------------------------

// 
// TASK
//

gulp.task('clean', function() 
{
	return del([config.buildAssets, config.buildHTML],{force:true}).then(paths => 
    {
    	console.log('Files and folders that would be deleted:\n', paths.join('\n'));
	}); 
});


// -------------------------------------
// DEFAULT
// -------------------------------------

// 
// TASK
//

gulp.task('default', function () {
	// runSequence('clean',  ['styles-t7', 'styles-t7-min', 'styles-blue', 'styles-red', 'styles-red-live', 'styles-red-live-static'], ['redirect-html', 'html', 'images', 'videos', 'fonts', 'webpack'], ['static_css', 'static_js', 'static_images'], ['watch', 'browserSync']);
	runSequence('clean', ['styles-t7', 'styles-t7-guide', 'styles-blue', 'styles-red', 'styles-red-live' ], ['redirect-html', 'html', 'images', 'videos', 'fonts', 'webpack'],['watch', 'browserSync']);

});

gulp.task('t7', function () {
	runSequence('clean', ['styles-t7', 'styles-t7-guide'], ['redirect-html', 'html', 'images', 'videos', 'fonts', 'webpack'],['watchT7', 'browserSync']);
});

// -------------------------------------
// Production build
// -------------------------------------

// 
// TASK
//
// Note: Minifies Javascript, CSS, and doesnt run browserSync
//

gulp.task('production', function () 
{
  runSequence('clean', ['styles-t7'], ['redirect-html-dist', 'html', 'images', 'videos', 'fonts', 'webpack']);
});



// 
// -------------------------------------
//   HELPER FUNCTIONS
// -------------------------------------
// 

handleErrors = function(errorObject, callback) 
{
  notify.onError(errorObject.toString().split(': ').join(':\n')).apply(this, arguments);
  if (typeof this.emit === 'function') this.emit('end');
};


var ftpObj = { host: 'ftp.torpedo7.co.nz', user: 't7', pass: 'eXWy8NJNyjoe', remotePath: '/srv/www/torpedo7.com/docroot/NZtest/docs/assets/'}

gulp.task('deploy-build', ['production'], () => {
});

gulp.task('deploy', ['deploy-build'], function () {
	return gulp.src('dist/assets/**/*')
		.pipe(sftp(ftpObj));
});

gulp.task('deploy-prod', ['deploy-build'], function () {
	var ftpObjNZ = Object.assign({}, ftpObj);
	var ftpObjAU = Object.assign({}, ftpObj);
	ftpObjNZ.remotePath = '/srv/www/torpedo7.com/docroot/NZ/docs/assets/';
	ftpObjAU.remotePath = '/srv/www/torpedo7.com/docroot/AU/docs/assets/';
	return gulp.src('dist/assets/**/*')
		.pipe(sftp(ftpObjNZ))
		.pipe(sftp(ftpObjAU))
});